Object = {}
Object.__index = Object

function ccw(A, B, C)
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)
end

function intersect(A,B,C,D)
    return ccw(A,C,D) ~= ccw(B,C,D) and ccw(A,B,C) ~= ccw(A,B,D)
end

function rotate(origin, point, angle)
    ox = origin.x
    oy = origin.y
   
    px = point.x
    py = point.y    

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)

    return Vector:create(qx, qy)
end

function Object:create(x, y, owidth, oheight, angle, status, weight)
    local object = {}
    setmetatable(object, Object)
    p = 0
    object.velocity = Vector:create(0,0)
    object.acceleration = Vector:create(0,0.06)
    object.position = Vector:create(x,y)
    object.status = status
    object.width = owidth
    object.height = oheight
    angle = (math.pi / 180) * angle
    object.angle = angle
    object.weight = weight or 0.01

    
    object.A = Vector:create(0,0)
    object.B = Vector:create(0,0)
    object.C = Vector:create(0,0)
    object.D = Vector:create(0,0)

    object.aAcceleration = 0.0002
    object.aVelocity = 0

    object:Bbox()
    return object
end

function Object:update()
    if self.status == "dinamic" then
        self.velocity:add(self.acceleration)
        self.position:add(self.velocity)
        self.aVelocity = self.aVelocity + self.aAcceleration
        
        self.angle = self.angle + self.aVelocity
        self.aVelocity = self.aVelocity + self.aAcceleration
      --  self.acceleration:mul(0)
    end 

    self:Bbox()

end

function Object:NForce(other)
  
    local addition =  self.weight * math.cos(other.angle - self.angle)
  
  --  local N = self.aVelocity * addition
    
    self.angle = self.angle  + addition
   
    nacceleration =  rotate(Vector:create(0,0), self.velocity,  other.angle) - self.acceleration
    print(nacceleration)
    --nacceleration:mul(addition)
    print(nacceleration)
    self.position:add(nacceleration)
    self.position = self.position - self.velocity

    

    return N
end

function Object:Bbox()
    local point_Ax = self.position.x - self.width/2
    local point_Ay = self.position.y - self.height/2

    local point_A = Vector:create(point_Ax, point_Ay)
    local point_B = point_A + Vector:create(self.width, 0)
    local point_C = point_A + Vector:create(self.width, self.height)
    local point_D = point_A + Vector:create(0, self.height)

    self.A = rotate(self.position, point_A, self.angle)
    self.B = rotate(self.position, point_B, self.angle)
    self.C = rotate(self.position, point_C, self.angle)
    self.D = rotate(self.position, point_D, self.angle)
end


function Object:intersection(other)
 
    local a = intersect(self.A,self.B,other.A, other.B)
    local b = intersect(self.A,self.B,other.B, other.C)
    local c = intersect(self.A,self.B,other.C, other.D)
    local c1 = intersect(self.A,self.B,other.D, other.A)

    local d = intersect(self.B,self.C,other.A, other.B)
    local e = intersect(self.B,self.C,other.B, other.C)
    local f = intersect(self.B,self.C,other.C, other.D)
    local f1 = intersect(self.B,self.C,other.D, other.A)

    local g = intersect(self.C,self.D,other.A, other.B)
    local h = intersect(self.C,self.D,other.B, other.C)
    local i = intersect(self.C,self.D,other.C, other.D)
    local j = intersect(self.C,self.D,other.D, other.A)

    
    local k = intersect(self.D,self.A,other.A, other.B)
    local l = intersect(self.D,self.A,other.B, other.C)
    local m = intersect(self.D,self.A,other.C, other.D)
    local n = intersect(self.D,self.A,other.D, other.A)
    

    return a or b or c or d or e or f or g or d or e or c1 or f1 or h or h or i or j or k or l or m or n
end

function Object:draw1()
    love.graphics.push()
    r, g, b, a = love.graphics.getColor()
    love.graphics.translate(self.position.x, self.position.y)
    --love.graphics.rotate(0)
    love.graphics.rotate(self.angle)
    
    love.graphics.rectangle( "fill", self.position.x - self.width/2, self.position.y - self.height/2, self.width, self.height)
    love.graphics.setColor(1, 0, 0)
    love.graphics.circle( "fill", self.position.x, self.position.y , 5)
    love.graphics.setColor(1, 1, 0)
    love.graphics.circle( "fill", self.A.x, self.A.y , 5)
    love.graphics.setColor(0, 1, 0)
    love.graphics.circle( "fill", self.B.x, self.B.y , 5)
    love.graphics.setColor(0, 0, 1)
    love.graphics.circle( "fill", self.C.x, self.C.y , 5)
    love.graphics.setColor(1, 0, 0)
    love.graphics.circle( "fill", self.D.x, self.D.y , 5)

    love.graphics.setColor(r, g, b, a)
    love.graphics.pop()
end


function Object:draw()

    love.graphics.push()
    r, g, b, a = love.graphics.getColor()
    love.graphics.translate(self.position.x, self.position.y)
    love.graphics.rotate(0)
   -- angle = math.atan2(self.velocity.y, self.velocity.x)    
    love.graphics.rotate(self.angle)
    love.graphics.rectangle("fill", -self.width/2, -self.height /2, 
                                           self.width, self.height)
 
    love.graphics.pop()

    
    love.graphics.setColor(1, 0, 0)
    love.graphics.circle( "fill", self.position.x, self.position.y , 5)
  
    love.graphics.setColor(r, g, b, a)

   -- love.graphics.rectangle( "fill", self.position.x - self.width/2, self.position.y - self.height/2, self.width, self.height)
  

end