SParticleSystem = {}
SParticleSystem.__index = SParticleSystem


function SParticleSystem:create(n)
    local system = {}
    setmetatable(system, SParticleSystem)
    system.origin = Vector:create(x, y)
    system.n = n or 10
    system.sparticles = {}
    system.index = 0

    system.dead = nil
 
    return system
end


function SParticleSystem:draw()
    for k, v in pairs(self.sparticles) do
     --   print(v.location.x)
        v:draw()
       
    end
end

function SParticleSystem:update()
    if #self.sparticles < self.n then
        self.sparticles[self.index] = SParticle:create(math.random(0,width - 100), 
        math.random(0,width - 100), math.random(10, 100))
        --print(self.sparticles[self.index].location.x)                    
        self.index = self.index + 1
    end

    for k, v in pairs(self.sparticles) do
        if v.destroy then
            self.dead = v
            v = SParticle:create(math.random(0,width - 100), 
            math.random(0,width - 100),  math.random(10, 100))   
            self.sparticles[k] = v
            
        end
        v: update()
    end
end


function SParticleSystem:press(xm, ym)
    for k, v in pairs(self.sparticles) do
        if v:onSParticle(xm,ym) then
            v.smalling = true
        end
    end
end