Particle = {}
Particle.__index = Particle

function Particle: create(x, y, size)
    local particle  = {}
    setmetatable(particle, Particle)

    particle.position = Vector:create(x, y)
    particle.velocity = Vector:create(0,math.random(20, 120)/100)

    particle.image = love.graphics.newImage("snowflake.png") 
    particle.lifespan = 1000
    particle.decay = math.random(100,200)/100
    particle.acceleration = Vector:create(0, 0.2)
    particle.size = size
    return particle
end


function Particle:update()
    if self.lifespan > 0 then
        self.acceleration = Vector:create(math.random(-200,200)/25000,math.random(0,200)/25000)
        self.velocity:add(self.acceleration)
        self.position:add(self.velocity)
       -- self.acceleration:mul(0)
    end
    self.lifespan = self.lifespan - self.decay
end

function Particle:applyForce(force)
    self.acceleration:add(force)
end

function Particle:draw()
    r,g,b,a = love.graphics.getColor()
    if self.lifespan > 0 then
        love.graphics.setColor(0.1, 0.9, 0.9, self.lifespan / 1000) 
        love.graphics.draw(self.image, self.position.x, self.position.y, math.rad(90), 0.6, 0.6)
           
    --    love.graphics.circle( "fill", self.position.x, self.position.y, self.lifespan/100 * self.size)
    end
   
        -- love.graphics.line(self.p1.x, self.p1.y, self.p2.x, self.p2.y)
    love.graphics.setColor(r,g,b,a)
end

function Particle:isDead()
    return self.position.y > height
  end