Path = {}
Path.__index = Path


function Path:create(points, d)
    local path = {}
    setmetatable(path, Path)
    path.points = points or self:generate()

    path.d = 5
    return path
end



function Path:generate()

    local x = width/2
    local y = height/2
    local points = {}
    for i = 0, 1000 do
        local t = i / 10 * math.pi
        local dx = x + t * math.cos(t) * 5
        local dy = y+ t * math.sin(t) * 5
        local coord = {dx, dy}
        table.insert(points, coord)
    end
    return points
end

function Path:draw()
    local r,g,b,a = love.graphics.getColor()
    love.graphics.setLineWidth(self.d)
    love.graphics.setColor(0.31, 0.31, 0.31, 0.7)
    for i  = 2, #self.points do
        local start = self.points[i-1]
        local stop = self.points[i]
        love.graphics.line(start[1], start[2], stop[1], stop[2])
        love.graphics.setBlendMode("replace")
        love.graphics.circle("fill", start[1], start[2], self.d/2)
        love.graphics.circle("fill", stop[1], stop[2], self.d/2)
    end
    love.graphics.setColor(r,g,b,a)
end