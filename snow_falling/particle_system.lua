ParticleSystem = {}
ParticleSystem.__index = ParticleSystem

function ParticleSystem:create(x,y, n)
    local system = {}
    setmetatable(system, ParticleSystem)
    system.origin = Vector:create(x, y)
    system.n = n
    system.count = 0
    system.particles = {}
    system.index = 0
    return system
end

function ParticleSystem:update()
    if self.n > #self.particles then  
        local x = math.random (0, width)
        local y = math.random (-60, 10)
        local particle = Particle:create(x, y, 20)
        table.insert( self.particles, particle)
    end

    for k, v in pairs(self.particles) do
        if v then
            if v:isDead() then
                table.remove(self.particles, k)
            end
            v:update()
        end
    end

end


function ParticleSystem:draw()
    for k, v in pairs(self.particles) do
        v:draw()
    end

end
