require("vector")
require("vehicle")
require("path")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    path = Path:create()
    vehicles = {}
    for i = 1, 200 do 
       veh =  Vehicle:create(math.random(width/2 - 300, width/2 + 300),math.random(height/2 - 300, height/2 + 300))
       veh.maxSpeed = math.random(100, 300)/100
       table.insert(vehicles, veh)
    end


end

function love.update(dt)
    for i = 1, #vehicles do 
        vehicles[i]:update(path)
    end
end

function love.draw()
  --  path:draw()

  for i = 1, #vehicles do 
    vehicles[i]:draw()
end

end
