Vehicle = {}
Vehicle.__index = Vehicle

function Vehicle:create(x, y)
    local vehicle = {}
    setmetatable(vehicle, Vehicle)

    vehicle.position = Vector:create(x, y)
    vehicle.st_position =  Vector:create(x, y)
    vehicle.velocity = Vector:create(0, 0)
    vehicle.acceleration = Vector:create(0,0)
    vehicle.r = 5
    vehicle.vertices = {0, -vehicle.r * 2, 
                        -vehicle.r, vehicle.r * 2, 
                        vehicle.r, vehicle.r * 2}

    vehicle.maxSpeed = 2
    vehicle.maxForce = 2
    vehicle.wtheta = 0
    vehicle.target = Vector:create(0,0)
    vehicle.i = 1
    return vehicle
end



function Vehicle:follow(path)    
    path = path.points
    self.target = Vector:create(path[self.i][1],path[self.i][2]) 
    self:seek(self.target)
    if self.position:distTo(self.target) <= 2 then
        self.i = self.i + 1
    end    
    if self.i == #path then
        self.i = 1
        self.location = Vector:create(width/2, height/2)
    end
end

function Vehicle:update(path)
    self:follow(path)
    self.velocity:add(self.acceleration)
    self.velocity:limit(self.maxSpeed)
    self.position:add(self.velocity)
    self.acceleration:mul(0)
end

function Vehicle:applyForce(force)
    self.acceleration:add(force)
end

-- ищет крусор
function Vehicle:seek(target)
    local desired = target - self.position
    local mag = desired:mag() 
    --print(mag)
    if mag == 0 then
        return
    end
    desired:norm()
    desired:mul(self.maxSpeed)
    
    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    print(steer)
    self:applyForce(steer)
end


function getNormal(p, a, b)
    local ap = p - a
    local ab = b - a
    ab:norm()
    ab:mul(ap:dot(ab))
    local point = a + ab
    return point
end


-- убегает от курсора
function Vehicle:flee(target)
    local desired = target - self.position
    local mag = desired:mag() 
    desired:norm()
    desired:mul(-1)
    if mag < 100 then
        desired:mul(self.maxSpeed)
    else
        local m = math.map(mag, 100, 200, self.maxSpeed, 0)
        desired:mul(m)
    end

    local steer = self.velocity
    steer:limit(self.maxForce)
    self:applyForce(steer)
end

function Vehicle:wander()
    local rwander = 25
    local dwander = 80
    self.wtheta = self.wtheta + love.math.random(-30, 30) / 100
    local pos = self.velocity:copy()
    pos:norm()
    pos:mul(dwander)
    pos:add(self.position)

    local h = self.velocity:heading()
    local offset = Vector:create(rwander * math.cos(self.wtheta + h), 
                                rwander * math.sin(self.wtheta + h))

    local target = pos + offset
    self:seek(target)

    love.graphics.circle("line", pos.x, pos.y, rwander)
    love.graphics.circle("fill", target.x, target.y, 4)
end

function Vehicle:borders()
    if self.position.x < -self.r then
        self.position.x = width + self.r
    end
    if self.position.y < -self.r then
        self.position.y = height + self.r
    end
    if self.position.x > width + self.r then
        self.position.x = -self.r
    end
    if self.position.y > height + self.r then
        self.position.y = -self.r
    end
end

function Vehicle:on_start_position()
    if self.position.x < -self.r then
        self.position = self.st_position
    end

    if self.position.y < -self.r then
        self.position = self.st_position
    end
    if self.position.x > width + self.r then
        self.position = self.st_position
    end
    if self.position.y > height + self.r then
        self.position = self.st_position
    end
end

function Vehicle:boundaries()
    local desired = nil

    if self.position.x < d then
        desired = Vector:create(self.maxSpeed, self.velocity.y)
    elseif self.position.x > width - d then
        desired = Vector:create(-self.maxSpeed, self.velocity.y)
    end

    if self.position.y < d then
        desired = Vector:create(self.velocity.x, self.maxSpeed)
    elseif self.position.y > height - d then
        desired = Vector:create(self.velocity.x, -self.maxSpeed)
    end

    if desired then
        desired:norm()
        desired:mul(self.maxSpeed)
        local steer = desired - self.velocity
        steer:limit(self.maxForce)
        self:applyForce(steer)
    end
end

--function Vehicle:follow(flow)
 ----   local desired = flow:lookup(self.position)
    --desired:mul(self.maxSpeed)
  --  local steer = desired - self.velocity
  --  steer:limit(self.maxForce)
  --  self:applyForce(steer)
--end

function Vehicle:draw()
    local theta = self.velocity:heading() + math.pi / 2
    love.graphics.push()
    
    love.graphics.translate(self.position.x, self.position.y)
    love.graphics.rotate(theta)
    love.graphics.polygon("fill", self.vertices)

    love.graphics.pop()

  --  love.graphics.circle("fill", self.target.x, self.target.y, 4)
end