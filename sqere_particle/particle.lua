Particle = {}
Particle.__index = Particle

function Particle: create(x,y, width)
    local particle  = {}
    setmetatable(particle, Particle)
    particle.location = Vector:create(x,y)
    particle.velocity = Vector:create(math.random(-200,200)/100,
                                        math.random(-200,200)/100)
    particle.lifespan = 100
    particle.decay = math.random(3, 10)/ 10
    particle.acceleration = Vector:create(0, 0.1)
    particle.width = width
    return particle
end

function Particle:update()
    self.velocity:add(self.acceleration)
    self.location:add(self.velocity)
    self.acceleration:mul(0)
    self.lifespan = self.lifespan - self.decay
end

function Particle:applyForce(force)
    self.acceleration:add(force)
end
function Particle: draw()
    love.graphics.rectangle("line", self.location.x, self.location.y, self.width, self.width)
end

function Particle: isDead()
    return self.lifespan < 0
  end