SParticle = {}
SParticle.__index = SParticle

function SParticle: create(x,y, width)
    local sparticle  = {}
    setmetatable(sparticle, SParticle)
    sparticle.location = Vector:create(x,y)
    sparticle.width = width or 100
    sparticle.width_orig = width or 100
    sparticle.min_width = 2
    sparticle.destroy = false
    sparticle.smalling = false
    return sparticle
end
function SParticle: update()
    if self.width > self.min_width and self.smalling == true then
        self.width = self.width - 1
        self.location.x = self.location.x + 0.5
        self.location.y = self.location.y + 0.5
    end

    if self.width <= self.min_width then
        self.destroy = true
    end
end
        
function SParticle: draw()
    love.graphics.rectangle("line", self.location.x, self.location.y, self.width, self.width)
end


function SParticle: onSParticle(xm, ym)
    return xm < (self.location.x + self.width) and
        (xm > self.location.x) and
        (ym < self.location.y + self.width) and
        (ym > self.location.y) 
end