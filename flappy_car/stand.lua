Stand = {}
Stand.__index = Stand

function Stand:create(oheight, velocity, type)
    local stand = {}
    setmetatable(stand, Stand)

    stand.height = oheight
    stand.width = 100
    stand.type = type
    if type == 1 then
        stand.position = Vector:create(width-stand.width/2, height-stand.height/2)
    elseif type == -1 then
        stand.position = Vector:create(width-stand.width/2, stand.height/2)
    end
    stand.velocity = velocity
   
    stand.is_alive = 1
    stand.image = love.graphics.newImage("assets/house.png") 
    

    bird.A = Vector:create(0,0)
    bird.B = Vector:create(0,0)
    bird.C = Vector:create(0,0)
    bird.D = Vector:create(0,0)
 

    return stand
end

function Stand:update()
   self.position = self.position - self.velocity  
   if self.position.x <= 0 then
        self.is_alive = -1
   end  
end

function ccw(A, B, C)
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)
end

function intersect(A,B,C,D)
    return ccw(A,C,D) ~= ccw(B,C,D) and ccw(A,B,C) ~= ccw(A,B,D)
end


function Stand:bbox()
    local point_Ax = self.position.x - self.width/2
    local point_Ay = self.position.y -self.height/2

   self.A = Vector:create(point_Ax, point_Ay)
   self.B = self.A  + Vector:create(self.width, 0)
   self.C = self.A  + Vector:create(self.width, self.height)
   self.D = self.A  + Vector:create(0, self.height)

end

function Stand:define_bird(bird)
    self:bbox()
    local a = intersect(self.A,self.B,bird.A, bird.B)
    local b = intersect(self.A,self.B,bird.B, bird.C)
    local c = intersect(self.A,self.B,bird.C, bird.D)
    local c1 = intersect(self.A,self.B,bird.D, bird.A)

    local d = intersect(self.B,self.C,bird.A, bird.B)
    local e = intersect(self.B,self.C,bird.B, bird.C)
    local f = intersect(self.B,self.C,bird.C, bird.D)
    local f1 = intersect(self.B,self.C,bird.D, bird.A)

    local g = intersect(self.C,self.D,bird.A, bird.B)
    local h = intersect(self.C,self.D,bird.B, bird.C)
    local i = intersect(self.C,self.D,bird.C, bird.D)
    local j = intersect(self.C,self.D,bird.D, bird.A)

    
    local k = intersect(self.D,self.A,bird.A, bird.B)
    local l = intersect(self.D,self.A,bird.B, bird.C)
    local m = intersect(self.D,self.A,bird.C, bird.D)
    local n = intersect(self.D,self.A,bird.D, bird.A)
    

    return a or b or c or d or e or f or g or d or e or c1 or f1 or h or h or i or j or k or l or m or n
end

function Stand:draw()


    love.graphics.push()
    --love.graphics.setColor(252/255, 163/255, 183/255)
    if self.type == 1 then
        love.graphics.rectangle("fill", self.position.x-self.width/2, height - self.height, 
                                           100, self.height) 
        love.graphics.draw(self.image, self.position.x - self.width/2, height - self.height)  
    elseif self.type == -1 then
        love.graphics.rectangle("fill", self.position.x-self.width/2, self.position.y-self.height/2, 
            100, self.height) 
        love.graphics.draw(self.image, self.position.x - self.width/2, -(height/2 - self.height))  
    end
  
  
   --  self:bbox()
  --  love.graphics.circle("fill",  self.A.x ,  self.A.y, 5)  
   -- love.graphics.circle("fill",  self.B.x ,  self.B.y, 5) 
   -- love.graphics.circle("fill",  self.C.x ,  self.C.y, 5) 
   -- love.graphics.circle("fill",  self.D.x ,  self.D.y, 5) 
   -- love.graphics.setColor(r, g, b, a) 
    love.graphics.pop()  
   
  
    
end


