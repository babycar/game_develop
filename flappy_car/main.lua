require("vector")
require("bird")
require("stand")
require("stand_controller")


function love.load()
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()   

   bird = Bird:create(width/2, height/2, 40, 50, 90)
   velocity = Vector:create(1,0)
 
   stand_controller = StandController:create(350, velocity)

   image = love.graphics.newImage("assets/phone.png") 
   love.graphics.setNewFont("assets/18392.otf", 50)
   bird_move = love.audio.newSource("assets/fly2.wav", "static")
   bam = love.audio.newSource("assets/bam.mp3", "static")
   game_status = 0
   game_end = 0

end

function love:update(dt)

        if game_status~=0 then
        bird:update_down()
        bird:is_down()
        if bird.up_velocity~=0 then
            stand_controller:update()
            stand_controller:define_bird(bird)
        else 
            if game_end == 0 then
                bam:play("assets/bam.mp3")
            end
            game_status = 2
            game_end = 1
        end
    end
    
end



function love.draw()
    love.graphics.draw(image)     
    stand_controller:draw()
    bird:draw()

   if game_status == 0 then
        love.graphics.printf("PRESS 'SPACE' TO START", 0,200, width, "center")
   end

   if game_status == 2 then
        love.graphics.printf("YOU ARE LOOS", 0,200, width, "center")
   end

   love.graphics.printf(stand_controller.points, 0,100, width, "center")
   
  
end

function love.keypressed(key)
  if key == "space" and game_end == 0 then
    bird:update_up()
    bird_move:play("assets/fly.mp3")
    game_status = 1 
  end

end


