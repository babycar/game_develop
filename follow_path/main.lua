require("vector")
require("vehicle")
require("path")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    points = {Vector:create(0, 0),  Vector:create(100,50), Vector:create(200,200), Vector:create(600, 400),
            Vector:create(700, 500),  Vector:create(width, 400) }
    path = Path:create( points)

    vehicle1 = Vehicle:create(0,100)
    vehicle1.velocity.x = 2
    vehicle1.maxForce = 0.9
    vehicle1.maxSpeed = 2.5

    vehicle2 = Vehicle:create(400,400)
    vehicle2.velocity.x = 0.2
    vehicle2.maxForce = 0.7
    vehicle2.maxSpeed = 2

end

function love.update(dt)

    vehicle1:borders()
    vehicle2:borders()
    vehicle1:update(path)
    vehicle2:update(path)

end

function love.draw()
    path:draw()
    vehicle1:draw()
    vehicle2:draw()
end
