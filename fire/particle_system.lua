ParticleSystem = {}
ParticleSystem.__index = ParticleSystem

function ParticleSystem:create(x,y, n)
    local system = {}
    setmetatable(system, ParticleSystem)
    system.origin = Vector:create(x, y)
    system.color = math.random(10, 255)/255
    system.n = n
    system.count = 0
    system.particles = {}
    system.index = 0
    return system
end

function ParticleSystem:update()
    if self.n > #self.particles then  
        local particle = Particle:create(self.origin.x,self.origin.y, 20, self.color)
        table.insert( self.particles, particle)
    end

    for k, v in pairs(self.particles) do
        if v then
            if v:isDead() then
                table.remove(self.particles, k)
            end
            v:update()
        end
    end

end


function ParticleSystem:draw()
    for k, v in pairs(self.particles) do
        v:draw()
    end

end
