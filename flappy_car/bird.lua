Bird = {}
Bird.__index = Bird

function Bird:create(x, y, owidth, oheight, angle)
    local bird = {}
    setmetatable(bird, Bird)

  --  bird.velocity = Vector:create(0,0)
  --  bird.acceleration = Vector:create(0,0.1)
    bird.image_stay = love.graphics.newImage("assets/babycar1.png") 
    bird.image_fly = love.graphics.newImage("assets/babycar2.png") 
    bird.image = bird.image_stay
    bird.position = Vector:create(x,y)
    bird.status = status
    bird.width = owidth
    bird.height = oheight
    bird.angle = angle
  
    bird.A = Vector:create(0,0)
    bird.B = Vector:create(0,0)
    bird.C = Vector:create(0,0)
    bird.D = Vector:create(0,0)

    bird.up_velocity = 65
    bird.down_velocity = 3


    return bird
end

function Bird:update_up()


    if self.angle <= 100 and self.up_velocity~=0 then
        self.angle = self.angle+30
    end
   self.position = self.position + Vector:create(0,-self.up_velocity)

end

function Bird:update_down()
    
    if self.angle >= 0 then
        self.angle = self.angle - 1
    end
    
    self.position = self.position + Vector:create(0,self.down_velocity)

end
function Bird:draw()

    love.graphics.push()
    r, g, b, a = love.graphics.getColor()
    love.graphics.translate(self.position.x, self.position.y)
    love.graphics.rotate(0)
   -- angle = math.atan2(self.velocity.y, self.velocity.x)    
    angle = (math.pi / 180) * self.angle   
    love.graphics.rotate(-angle)
    --love.graphics.rotate(self.angle)
   -- love.graphics.rectangle("fill", -self.width/2, -self.height /2, 
     --                                      self.width, self.height) 
   if self.angle > 90 then
        self.image = self.image_fly
   end
   love.graphics.draw(self.image, -self.width/2, -self.height/2) 
   self.image = self.image_stay
   self:bbox()
   
    love.graphics.pop() 
   -- love.graphics.setColor(1, 0, 0)
    --love.graphics.circle("fill",  self.A.x ,  self.A.y, 5)  
    --love.graphics.circle("fill",  self.B.x ,  self.B.y, 5) 
    --love.graphics.circle("fill",  self.C.x ,  self.C.y, 5) 
    --love.graphics.circle("fill",  self.D.x ,  self.D.y, 5) 
    love.graphics.setColor(r, g, b, a) 
    
end

function rotate(origin, point, angle)
    ox = origin.x
    oy = origin.y
   
    px = point.x
    py = point.y    

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)

    return Vector:create(qx, qy)
end

function Bird:bbox()
    local point_Ax = self.position.x - self.width/2
    local point_Ay = self.position.y - self.height/2

    local point_A = Vector:create(point_Ax, point_Ay)
    local point_B = point_A + Vector:create(self.width, 0)
    local point_C = point_A + Vector:create(self.width, self.height)
    local point_D = point_A + Vector:create(0, self.height)

    self.A = rotate(self.position, point_A, math.rad(-self.angle))
    self.B = rotate(self.position, point_B, math.rad(-self.angle))
    self.C = rotate(self.position, point_C, math.rad(-self.angle))
    self.D = rotate(self.position, point_D, math.rad(-self.angle))

end


function Bird:is_down()
    if self.position.y > 550 then
        self.up_velocity = 0
        self.down_velocity = 0
    end
end