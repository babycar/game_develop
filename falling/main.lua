require("vector")
require("object")



function love.load()
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()   

   slide = Object:create(width/2, height/2, 50, 280, 60, "static")
   cube = Object:create(width/2, height/2 - 300, 50, 50, 0, "dinamic", 0.05)
end

function love:update(dt)
    cube:update()
end



function love.draw()
   
    if cube.status ~= "static" and slide:intersection(cube)==true then
        --cube.status = "static"
        cube:NForce(slide)
    end
    slide:draw()
    cube:draw()
  
end

function love.keypressed(key)
    if key == "space" and slide.status == "static" then
        cube.status = "dinamic"
    elseif key == "space" and cube.status == "dinamic" then
        cube.status = "static"
    end

end


