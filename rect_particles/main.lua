require("vector")
require("particle")
require("repeller")
require("particle_system")

function math.dist(x1, y1, x2, y2)
  return ((x2 - x1)^2 + (y2 - y1)^2)^0.5
end

function love.load()
  width = love.graphics.getWidth()
  height = love.graphics.getHeight()
  textures = {}
  textures.heart = love.graphics.newImage("heart.png")
  textures.fusion = love.graphics.newImage("texture.png")
  system = ParticleSystem:create(width/2,height/2, 50)
end

function love.draw()
  system:draw()
end

function love.update(dt)
  system:update()
end


function love.keypressed(key) 
end



function love.mousepressed(x, y, button, istouch)
  if button == 1 then
      system:clicked(x, y)
  end
end