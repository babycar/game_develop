Particle = {}
Particle.__index = Particle

function Particle: create(x, y, size, color)
    local particle  = {}
    setmetatable(particle, Particle)
    local x_add = math.random(-1400,1400)/1000
    local y_add = math.random(-1400,1400)/1000
    particle.position = Vector:create(x+x_add,y+y_add)
    particle.velocity = Vector:create(math.random(-200,200)/1000,   math.random(-200,200)/1000)
    particle.color_red = color
    particle.lifespan = 100
    particle.decay = 1
    particle.acceleration = Vector:create(0, -0.03)
    particle.size = size
    return particle
end


function Particle:update()
    if self.lifespan > 0 then
       -- self.acceleration = Vector:create(math.random(),math.random())
        self.velocity:add(self.acceleration)
        self.position:add(self.velocity)
       -- self.acceleration:mul(0)
    end
    self.lifespan = self.lifespan - self.decay
end

function Particle:applyForce(force)
    self.acceleration:add(force)
end

function Particle:draw()
    r,g,b,a = love.graphics.getColor()
    if self.lifespan > 0 then
        love.graphics.setColor(self.color_red, 0.25, 0.13, self.lifespan / 100)        
        love.graphics.circle( "fill", self.position.x, self.position.y, self.lifespan/100 * self.size)
    end
   
        -- love.graphics.line(self.p1.x, self.p1.y, self.p2.x, self.p2.y)
    love.graphics.setColor(r,g,b,a)
end

function Particle:isDead()
    return self.lifespan < 0
  end