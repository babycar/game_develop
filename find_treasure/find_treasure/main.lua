require "world"
require "player"
require "pqueue"
require "maze"

function loadTextures()
    env = {}
    env.tileset = love.graphics.newImage("assets/RogueEnvironment16x16.png")

    local quads = {
        {0,  5*16,  0*16}, -- floor v1
        {1,  6*16,  0*16}, -- floor v2
        {2,  7*16,  0*16}, -- floor v3
        {3,  0*16,  0*16}, -- upper left corner
        {4,  3*16,  0*16}, -- upper right corner
        {5,  0*16,  3*16}, -- lower left corner
        {6,  3*16,  3*16}, -- lower right corner
        {7,  2*16,  0*16}, -- horizontal
        {8,  0*16,  2*16}, -- vertical
        {9,  1*16,  2*16}, -- up
        {10, 2*16,  3*16}, -- down
        {11, 2*16,  1*16}, -- left
        {12, 1*16,  1*16}, -- right
        {13, 2*16,  2*16}, -- down cross
        {14, 1*16,  3*16}, -- up cross
        {15, 3*16,  1*16}, -- left cross
        {16, 0*16,  1*16}, -- right cross
        {17, 3*16, 14*16}, -- spikes
        {18, 5*16, 13*16} -- coin
    }
    env.textures = {}
    for i = 1, #quads do
        local q = quads[i]
        env.textures[q[1]] = love.graphics.newQuad(q[2], q[3], 16, 16, env.tileset:getDimensions())
    end

    pl = {}
    pl.tileset = love.graphics.newImage("assets/RoguePlayer_48x48.png")
    pl.textures = {}
    for i = 1, 6 do
        pl.textures[i] = love.graphics.newQuad((i - 1) * 48, 48 * 2, 48, 48, pl.tileset:getDimensions())
    end

end

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    loadTextures()

    world = World:create()
    scaleX = width / (world.width * 16)
    scaleY = height / (world.height * 16)

    world:placeObjects()
    player = world.player

    known_path = {}
-- Place code here

end

function love.update(dt)
    player:update(dt, world)
    world:update(player)
    seek(world:getEnv())
end

function seek(env)
    print(env.position[1], env.position[2], env.left, env.right, env.up, env.down, env.coin)
    local directions = {}
    if not env.left then
        table.insert(directions, "left")
    end
    if not env.right then
        table.insert(directions, "right")
    end
    if not env.up then
        table.insert(directions, "up")
    end
    if not env.down then
        table.insert(directions, "down")
    end

    if env.coin ~= "underfoot" then
        local coord = {env.position[1], env.position[2]}
        table.insert(known_path, coord)
        priorety = sort_priorety(directions, coord)
        world:move(priorety)

        for i = 1,#priorety do
            if in_array(directions, priorety[i]) then
            world:move(priorety[i])
            break
            end
        end
    end


end

function sort_priorety(directions, coord)
    local addition = {}
    addition["left"] = {-1,0}
    addition["right"] = {1,0}
    addition[ "up"] = {0,-1}
    addition ["down"] = {0,1}

    local pr = {}
    pr["left"] = 1000
    pr["right"] = 1000
    pr["up"] = 1000
    pr["down"] = 1000

    local priorety = "left"
    for i = 1, #directions do
        local xp = coord[1] + addition[directions[i]][1]
        local yp = coord[2] + addition[directions[i]][2]
        local coord2 = {xp, yp}

        pr[directions[i]] = coord_in_array(known_path, coord2)
    end

    for v, p in pairs(pr) do
        if p < pr[priorety] then
            priorety = v
        end
    end
    return priorety
end

function pprint(arr) 
    for i = 1, #arr do
        print(arr[i][1], arr[i][2])
    end
end

function coord_in_array(arr, elem)
  --  local add = false
    local count = 0
    for i = 1, #arr do
       if arr[i][1] == elem[1] and arr[i][2] == elem[2] then
        count = count + 1
      --  add = true  
       end
    end
    return count
end
function in_array(arr, elem)
    for i = 1, #arr do
        if arr[i] == elem then
            return true
        end
    end
    return false
end
function love.draw()
    love.graphics.scale(scaleX, scaleY)
    world:draw()
    player:draw(world)
end

function love.keypressed(key)
    if key == "left" then
        world:move("left")
    end
    if key == "right" then
        world:move("right")
    end
    if key == "up" then
        world:move("up")
    end
    if key == "down" then
        world:move("down")
    end
end