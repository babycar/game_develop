
require("vector")
require("sparticle")
require("sp_system")
require("particle")
require("particle_system")

function love.load()
  width = love.graphics.getWidth()
  height = love.graphics.getHeight()

  system = SParticleSystem:create(10)
  system_part = ParticleSystem:create(width/2, height/2, 10,100)
  --sparticle = SParticle:create(width/2, height/2, 100)
  delete = false
end

function love.draw()
  system:draw()
  system_part:draw()
  end

function love.update(dt) 
  --print(system.dead.x)
  if system.dead == nil then
    x = 0
    y = 0
  else

    x = system.dead.location.x
    y = system.dead.location.y
    area = system.dead.width_orig
    print(area)
    system_part = ParticleSystem:create(x, y, 5, area)
    system.dead = nil
  end

  system:update()
  system_part:update()
end



function love.mousepressed(x, y, button)  
  system: press(x,y)
end