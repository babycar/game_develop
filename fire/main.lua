require("particle")
require("vector")
require("particle_system")
function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    
   -- particle = Particle:create(width/2,height/2, 20)
    system_1 = ParticleSystem:create(width/2,height/2, 2000)
    system_2 = ParticleSystem:create(width/2 + 20,height/2+20, 2000)
    system_3 = ParticleSystem:create(width/2 - 20,height/2+20, 2000)
    system_4 = ParticleSystem:create(width/2,height/2+30, 2000)
end

function love:update()
  --  particle:update()
    system_1:update()
    system_2:update()
    system_3:update()
    system_4:update()
end

function love.draw()
 --  particle:draw()
   system_1:draw()
   system_2:draw()
   system_3:draw()
   system_4:draw()
end

function love.keypressed(key)
end