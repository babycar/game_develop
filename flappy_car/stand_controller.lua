StandController = {}
StandController.__index = StandController

function StandController:create(step, velocity)
    local stand_controller = {}
    setmetatable(stand_controller, StandController)

    stand_controller.step = step
    stand_controller.velocity = velocity
    stand_controller.accleration = Vector:create(0.005, 0)
    stand_controller.current_stands = {}

    stand_controller.stand_count = 0
    stand_controller.status = true
    stand_controller.points = 0
    return stand_controller
end

function StandController:update()


  
  if  self.stand_count == 0 or self.current_stands[self.stand_count].position.x <= (width - self.step) then
        local oheight1 = math.random(150, height/2)
        local oheight2 =  math.random(150, (height - oheight1)-150 )
        
        local stand_up = Stand:create(oheight1, self.velocity, -1)
        local stand_down = Stand:create(oheight2, self.velocity, 1)

        table.insert( self.current_stands, stand_up)
        table.insert( self.current_stands, stand_down)

        self.stand_count = self.stand_count + 2 
 end

  --self.velocity = self.velocity + self.accleration
  local points = 0
  for i = 1, self.stand_count do 
    self.current_stands[i]:update()
    if self.current_stands[i].position.x < (width/2) then
        points = points + 1
    end
    self.current_stands[i].velocity = stand_controller.velocity 
    
  end
  stand_controller.velocity =  stand_controller.velocity +  stand_controller.accleration
 -- print(points)
  self.points = points/2
--local to_delete = {}
--local count = 0
--for i = 1, #self.current_stands do     
  --  if self.current_stands[i].is_alive == -1 then
    --    print("----")
      --  table.insert( to_delete, i)
     ----   print(i)
       -- print("-----")
        --count = count + 1
    --end
--end
--if count~= 0 then
  --  self.stand_count = self.stand_count - count 
--end

--for i = 1, #to_delete do
 --   table.remove(self.current_stands, to_delete[i])
--end

end

function StandController:draw()

    for i = 1, self.stand_count do 
        self.current_stands[i]:draw()
    end
end

function StandController:define_bird(bird)
    for i = 1, self.stand_count do 
        --print(i)
        if self.current_stands[i]:define_bird(bird) then
            bird.up_velocity = 0
        end
    end
end
  